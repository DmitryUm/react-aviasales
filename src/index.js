import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import tickets from './data/tickets.json';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App tickets={tickets}/>, document.getElementById('root'));
registerServiceWorker();
