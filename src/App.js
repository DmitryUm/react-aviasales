import React, { Component } from 'react'

import ItemsList from './components/ItemsList.jsx'
import Form from './components/Form.jsx'

class App extends Component {
  state = {
    tickets: [],
    inputs: [
      {
        id: -1,
        label: 'Все',
        isChecked: true
      },
      {
        id: 0,
        label: 'Без пересадок',
        isChecked: false
      },
      {
        id: 1,
        label: '1 пересадка',
        isChecked: false
      },
      {
        id: 2,
        label: '2 пересадки',
        isChecked: false
      },
      {
        id: 3,
        label: '3 пересадки',
        isChecked: false
      }
    ]
  }

  componentDidMount() {
    this.setState({ tickets: this.props.tickets })
  }

  handleChange = id => {
    let inputs = this.state.inputs.slice()

    if (id === -1 && !inputs[0].isChecked) {
      inputs.map(input => (input.isChecked = false))
      inputs[0].isChecked = true

      this.setState({ inputs })
      return
    }

    if (id === -1 && inputs[0].isChecked) {
      inputs[0].isChecked = false
      inputs[1].isChecked = true

      this.setState({ inputs })
      return
    }

    inputs.map(input => {
      if (input.id === id) {
        input.isChecked = !input.isChecked
      }
      return input
    })
    inputs[0].isChecked = false

    this.setState({ inputs })
  }

  render() {
    const { tickets, inputs } = this.state

    const stops = inputs.reduce((arr, input) => {
      if (input.isChecked) arr.push(input.id)
      return arr
    }, [])

    return (
      <div className="App">
        <img src="img/logo.jpg" className="App__logo" alt="logo" />
        <main className="App__main">
          <Form inputs={inputs} handleChange={this.handleChange} />

          <ItemsList tickets={tickets} stops={stops} />
        </main>
      </div>
    )
  }
}

export default App
