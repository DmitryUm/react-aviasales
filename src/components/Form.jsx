import React from 'react'

import Checkbox from './Checkbox'

export default function Form({ inputs, handleChange }) {
  const only = inputs[0].isChecked

  return (
    <aside className="App__form  Form">
      <h4 className="Form__title">Количество пересадок</h4>
      <ul className="Form__list">
        {inputs.map(item => (
          <Checkbox
            key={item.id}
            only={only}
            handleChange={handleChange}
            {...item}
          />
        ))}
      </ul>
    </aside>
  )
}
