import React from 'react'

export default function Checkbox({ label, id, only, isChecked, handleChange }) {
  function toggleChange() {
    handleChange(id)
  }

  return (
    <li className="Form__item">
      <label className="Form__stops">
        <input
          type="checkbox"
          id={id}
          checked={isChecked}
          onChange={toggleChange}
        />
        {label}
        {id >= 0 && only && <span>Только</span>}
      </label>
    </li>
  )
}
