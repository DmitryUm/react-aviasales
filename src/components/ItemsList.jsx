import React from 'react'
import Item from './Item.jsx'

export default function ItemsList({ tickets, stops }) {
  const sortedTickets = tickets.slice().sort((a, b) => a.stops - b.stops)
  const filteredTickets = filterByStops(sortedTickets, stops)

  function filterByStops(tickets, stops) {
    if (stops[0] === -1) return tickets

    let filteredTickets = []

    stops.forEach(stop => {
      tickets.forEach(ticket => {
        if (ticket.stops === stop) filteredTickets.push(ticket)
      })
    })

    return filteredTickets
  }

  return (
    <section className="App__list">
      {filteredTickets.map((ticket, i) => <Item key={i} {...ticket} />)}
    </section>
  )
}
