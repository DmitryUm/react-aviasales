import React from 'react'

export default function Item({
  origin,
  origin_name,
  destination,
  destination_name,
  departure_date,
  departure_time,
  arrival_date,
  arrival_time,
  carrier,
  stops,
  price
}) {
  const departureDate = dateToString(departure_date)
  const arrivalDate = dateToString(arrival_date)

  function dateToString(date) {
    let arr = date.split('.').reverse()
    arr[1] -= 1

    let newDate = new Date(...arr)

    const options = {
      year: '2-digit',
      month: 'short',
      day: 'numeric',
      weekday: 'short'
    }

    newDate = newDate.toLocaleString('ru', options)

    return newDate[0].toUpperCase() + newDate.slice(1)
  }

  return (
    <div className="App__item  Item">
      <div className="Item__price">
        <img src={`img/${carrier}.jpg`} alt="" className="Item__logo" />
        <button className="Item__buy btn">
          Купить <span>за {price} Р</span>
        </button>
      </div>

      <div className="Item__fly Fly">
        <div className="Fly__time">
          <time className="Fly__time-departure">{departure_time}</time>
          <div className="Fly__stops">
            {stops === 0
              ? 'Без пересадок'
              : stops === 1 ? `${stops} пересадка` : `${stops} пересадки`}
          </div>
          <time className="Fly__time-arrival">{arrival_time}</time>
        </div>

        <div className="Fly__date">
          <div className="Fly__date-departure">
            {`${origin}, ${origin_name}`}
            <time>{departureDate}</time>
          </div>
          <div className="Fly__date-arrival">
            {`${destination_name}, ${destination}`}
            <time>{arrivalDate}</time>
          </div>
        </div>
      </div>
    </div>
  )
}
